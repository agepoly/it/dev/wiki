#!/usr/bin/env ruby
require 'rubygems'
require 'sinatra/base'
require 'rack'
require 'omniauth'
require 'omniauth-tequila'
require 'rack-rewrite'
require 'open3'

module Cst
 WIKIS = [
{web_path: '', disk_path: 'home', name: 'Home', remote: 'git@gitlab.com:agepoly/CdD.wiki.git', is_root: true},

{web_path: 'presidency', disk_path: 'presidency', name: 'Présidence', remote: 'git@gitlab.com:agepoly/presidency/Handbook.wiki.git'},

{web_path: 'it', disk_path: 'it', name: 'Informatique', remote: 'git@gitlab.com:agepoly/it/Handbook.wiki.git'},

{web_path: 'logistics', disk_path: 'logistics', name: 'Logistique', remote: 'git@gitlab.com:agepoly/logistics/Handbook.wiki.git'},



{web_path: 'representation/agepolytique', disk_path: 'representation/agepolytique', name: 'AGEPolytique', remote: 'git@gitlab.com:agepoly/representation/agepolytique/Handbook.wiki.git'},


{web_path: 'representation/external_relations', disk_path: 'representation/external_relations', name: 'Relations Externes', remote: 'git@gitlab.com:agepoly/representation/external_relations/Handbook.wiki.git'},


{web_path: 'commissions', disk_path: 'commissions', name: 'Commissions', remote: 'git@gitlab.com:agepoly/commissions/Handbook.wiki.git'},


{web_path: 'sport', disk_path: 'sport', name: 'Sport', remote: 'git@gitlab.com:agepoly/sport/Handbook.wiki.git'},


{web_path: 'animation', disk_path: 'animation', name: 'Animation', remote: 'git@gitlab.com:agepoly/animation/Handbook.wiki.git'},


{web_path: 'administration', disk_path: 'administration', name: 'Administration', remote: 'git@gitlab.com:agepoly/administration/Handbook.wiki.git'},

{web_path: 'communication', disk_path: 'communication', name: 'Communication', remote: 'git@gitlab.com:agepoly/communication/Handbook.wiki.git'},

  ]
  
 #presidency logistics representation/agepolytique representation/external_relations]
   def Cst.root
   WIKIS.each do |w|
    if defined?(w[:is_root]) && w[:is_root] == true
      return w
    end
    end
    end
    
   def Cst.findFor(path, predelimiter='/', postdelimiter = '/')
   to_check_later = nil
   WIKIS.each do |w|
    puts path
    if  w[:web_path] == ''
      to_check_later = w
      puts w
    elsif Regexp.new("#{predelimiter}#{w[:web_path]}#{postdelimiter}(.*)?") =~ path  
      puts w
      return w, '/'+Regexp.last_match(1)
     end
    end
    puts "#{postdelimiter}(.*)?}"
    if  Regexp.new("#{postdelimiter}(.*)?") =~ path
       puts 'hh'
       return to_check_later, '/'+(Regexp.last_match(1).nil? ? '' : Regexp.last_match(1))
    end
    return nil, nil
    end
end

module Gollum
  GIT_ADAPTER = "rugged"
  class Page
     def set_path(path)
       @path = path
     end
  end
  class File
     def set_path(path)
       @path = path
     end
  end
end

module Precious
  class App < Sinatra::Base
  include Cst
  get '/search' do
      @results = []
      @query   = params[:q] || ''
      @query.encode('UTF-8', 'binary', invalid: :replace, undef: :replace, replace: '')
      wikis = Cst::WIKIS
      wikis.each do |w|
      gollum_path = File.expand_path(File.dirname(__FILE__)+"/#{w[:disk_path]}")
      wiki     = Gollum::Wiki.new(gollum_path, {})
      # Sort wiki search results by count (desc) and then by name (asc)
      @results += wiki.search(@query).sort { |a, b| (a[:count] <=> b[:count]).nonzero? || b[:name] <=> a[:name] }.reverse
      end 
      @name    = @query
      mustache :search
    end
    get %r{
      /pages  # match any URL beginning with /pages
      (?:     # begin an optional non-capturing group
        /(.+) # capture any path after the "/pages" excluding the leading slash
      )?      # end the optional non-capturing group
    }x do |path|
      @path        = extract_path(path) if path
      wiki_options = settings.wiki_options.merge({ :page_file_dir => @path })
      @wiki         = Gollum::Wiki.new(settings.gollum_path, wiki_options)
      @results     = @wiki.pages
      @results     += @wiki.files if settings.wiki_options[:show_all]
      
      if defined?(env['WIKI']) && env['WIKI'][:is_root] == true
      wikis = Cst::WIKIS
      wikis.each do |w|
      cpage = @wiki.pages.first.dup
cpage.set_path("#{w[:web_path]}/fake.md")
      @results     << cpage
      end
      end
      @results     = @results.sort_by { |p| p.name.downcase } # Sort Results alphabetically, fixes 922
      @ref         = @wiki.ref
      mustache :pages
    end

    get '/fileview' do
      content = []
      
      gollum_path = File.expand_path(File.dirname(__FILE__)+"/#{Cst.root[:disk_path]}")
      wiki         = Gollum::Wiki.new(gollum_path, settings.wiki_options)
      content += wiki.pages
      content += wiki.files if settings.wiki_options[:show_all]
      
      
      wikis = Cst::WIKIS
      wikis.each do |w|
      unless w[:is_root] 
      gollum_path = File.expand_path(File.dirname(__FILE__)+"/#{w[:disk_path]}")
      wiki         = Gollum::Wiki.new(gollum_path, settings.wiki_options)
      pages = wiki.pages
      pages.each do |p| 
      
      p.set_path(::File.join(
      "#{w[:disk_path]}", 
      ::File.expand_path(::File.dirname(p.path), "/"), 
      "/fake"
      )) 
      
      end 
      
      content += pages
      if settings.wiki_options[:show_all]
      files = wiki.files 
      files.each do |p|
       p.set_path(::File.join(
      "#{w[:disk_path]}", 
      ::File.expand_path(::File.dirname(p.path), "/"), 
      "/fake"
      )) 
      end 
      content   += files
      end
      end
      end
      

      # must pass wiki_options to FileView
      # --show-all and --collapse-tree can be set.
      @results = Gollum::FileView.new(content, settings.wiki_options).render_files
      @ref     = wiki.ref
      mustache :file_view, { :layout => false }
    end
    
     post '/global-rename/*' do
    forbid unless @allow_editing

      wikip = wiki_page(params[:splat].first)
      halt 500 if wikip.nil?
      wiki   = wikip.wiki
      page   = wiki.paged(wikip.name, wikip.path, exact = true)
      rename = params[:rename]
      halt 500 if page.nil?
      halt 500 if rename.nil? or rename.empty?

      # Fixup the rename if it is a relative path
      # In 1.8.7 rename[0] != rename[0..0]
      if rename[0..0] != '/'
        source_dir                = ::File.dirname(page.path)
        source_dir                = '' if source_dir == '.'
        (target_dir, target_name) = ::File.split(rename)
        target_dir                = target_dir == '' ? source_dir : "#{source_dir}/#{target_dir}"
        rename                    = "#{target_dir}/#{target_name}"
      end
      target_wiki_infos = env[:target_wiki]
      target_name = env[:target_wiki_page_path]
      target_wiki = Gollum::Wiki.new(::File.dirname(__FILE__)+"/#{target_wiki_infos[:disk_path]}", settings.wiki_options)

commit_message =  {:message => "Move #{page.name} -> #{target_wiki_infos[:disk_path]} - Delete"}
target_commit_message = {:message => "Move #{rename} from #{wiki.path} - Create"}

    author_parameters  = session['gollum.author']
      commit_message.merge! author_parameters unless author_parameters.nil?
      target_commit_message.merge! author_parameters unless author_parameters.nil?
      
      committer = Gollum::Committer.new(wiki, commit_message)
      commit    = { :committer => committer }

      
      # need to ovveride layout for base path and wiki path info 
      # need to add cke
      
      
      # need ro correct redirextionnloop on /wiko/create (and potdnrially other) 
      
      #need to add the wikis
      
      
      
      
      target_committer = Gollum::Committer.new(target_wiki, target_commit_message)
      target_commit    = { :committer => target_committer }
      
      if !target_wiki.write_page(::File.basename(target_name, ".*"), page.format, page.raw_data, target_commit, ::File.dirname(target_name))
       halt 500
       return 
      end
      target_committer.commit
      success = wiki.delete_page(page, commit)
      committer.commit
      
      if !success
        # This occurs on NOOPs, for example renaming A => A
        halt 500
        return
      end
      
      page = target_wiki.paged(::File.basename(target_name, ".*"),::File.dirname(target_name))
      return if page.nil?
      redirect to("/#{target_wiki_infos[:web_path]}/#{page.escaped_url_path}")
    
   end
end
end

require 'gollum/app'

module Precious
  class App < Sinatra::Base
  #before do 
       # @base_url = env['WIKI'][:web_path] == '' ? url('/', false).chomp('/') : url("/#{env['WIKI'][:web_path]}/", false).chomp('/')
  #end
end
end
 
 module Precious
  module Views
    class Layout < Mustache
      include Cst
      def cosmetic_base_url
        return "/#{@env['WIKI'][:web_path]}" +@base_url unless @env['WIKI'][:web_path] == ''
        return @base_url
      end
      
      def wikis
         return Cst::WIKIS
      end
end
end
end
 
secret = 'hsiduhbbj83782887388hjdmebsuik0929agq'
 
class Deployer < Sinatra::Base
include Cst
include Open3
get '/install' do
   response = ''
   Cst::WIKIS.each do |wiki|
      path = File.expand_path(File.dirname(__FILE__)+"/#{wiki[:disk_path]}")
      response << "Cloned #{wiki[:name]}"
      stdout, stderr, status = Open3.capture3("git clone #{wiki[:remote]} #{path}")
      response << '\r\n' << stdout << '\r\n' << stderr << '\r\n'
      
   end
   
      if %r{r=(.*)} =~ env['QUERY_STRING']
    return [302, {'Location' => Regexp.last_match(1)}, [response + 'done']] 
   
   end 
   return [200, {}, [response + 'done']]
end 
   
   def Deployer.sync
   response = ''
   Cst::WIKIS.each do |wiki|
    path = File.expand_path(File.dirname(__FILE__)+"/#{wiki[:disk_path]}")
      response << "Synced #{wiki[:name]}"
      stdout, stderr, status = Open3.capture3(" cd #{path} && git pull && git push")
      response << '\r\n' << stdout << '\r\n' << stderr << '\r\n'
      return response
      end
     end
get '/sync' do
    response = sync

   if %r{r=(.*)} =~ env['QUERY_STRING']
    return [302, {'Location' => Regexp.last_match(1)}, [response + 'done']] 
   
   end 
   return [200, {}, [response + 'done']]
end

get '/toc' do
list = []
 Cst::WIKIS.each do |wiki|
     list << "* [#{wiki[:name]}](/#{wiki[:web_path]}/Home)" << '\n\r'
 end
  return [200, {}, list]
end
end

 class TaggingMiddleware
 def initialize(app, args = nil)
    @app = app
    if args && defined?(args[:wiki])
      @wiki = args[:wiki]
    end
  end

  def call(env)
    env['WIKI']=@wiki
    return @app.call(env)
    
  end
end

 class IsAuthMiddleware
 def initialize(app)
    @app = app
  end

  def call(env)
    return [302, {'Location' => '/auth/tequila'},['Login first']] unless env['rack.session'][:isauth]
    return @app.call(env)
    
  end
end

class OmniAuthCallbackController
  def self.call env
    return unless env['omniauth.auth'] != nil
    env['rack.session'][:isauth] = true
    env['rack.session']['gollum.author'] = {}
    env['rack.session']['gollum.author'][:name] = env['omniauth.auth'][:info][:name]   
    env['rack.session']['gollum.author'][:email] = env['omniauth.auth'][:info][:email]
    response = Deployer.sync
 return [302, {'Location' => '/'}, ['Login OK, sNc result : '+ response]]
  end
end


   use Rack::Session::Cookie, secret: secret
   use OmniAuth::Builder do provider :tequila, {:ssl => true, :host => 'tequila.epfl.ch', :service_name => 'Wiki AGEPOLY', :request_info => {:name => 'displayname', :email => 'email'}, :additional_parameters => {'require' => 'unit=AGEPOLY'}}
end

map '/auth/tequila/callback' do 
   run OmniAuthCallbackController
end

Gollum::Hook.register(:post_commit, :hook_id) do |committer, sha1|
        Deployer.sync
end


deployer_app = Sinatra.new(Deployer)
map "/deployer" do
   use IsAuthMiddleware
   run deployer_app
end

use Rack::Rewrite do
  Cst::WIKIS.each do |w|
  wiki = w[:web_path]
   unless wiki == ''
  r301 %r{/#{wiki}/search(.*)?}, "/search$1"
  r301 "/#{wiki}/fileview", "/fileview"
  rewrite %r{/(.*)/#{wiki}(/.*)?}, "/#{wiki}/$1$2"
  r301 %r{/#{wiki}(?:(/.*)/)?#{wiki}(/.*)?}, "/#{wiki}$1$2"
  end
 end
end


class GlobalRenameWatcher
 def initialize(app)
    @app = app
  end

  def call(env)
   if env['REQUEST_METHOD'] == 'POST'
   
   
     w, p = Cst.findFor(env['REQUEST_PATH'], '/', '/rename')
     req = Rack::Request.new(env)
     
     puts w
     puts req.params
     puts (defined?(req.params["rename"]))
     puts ((w != false))
     if w != false && defined?(req.params["rename"])
       
       puts 'test'
       
       tw, tp = Cst.findFor(req.params["rename"])
       puts tw
      if tw != false && tw != w
        
         env['PATH_INFO'] = env['PATH_INFO'].sub("rename", "global-rename")
        env['REQUEST_PATH'] = env['REQUEST_PATH'].sub("rename", "global-rename")
        env['REQUEST_URI'] = env['REQUEST_URI'].sub("rename", "global-rename")
        env[:target_wiki] = tw
        env[:target_wiki_page_path] = tp
       end
      end 
    puts env
    end
    return @app.call(env)
    
  end
end

use GlobalRenameWatcher

wiki_options = {:universal_toc => false}
wiki_options[:allow_editing] = true# # Equivalent to --no-edit
wiki_options[:live_preview] = false# Equivalent to --live-previe

wiki_options[:show_all] = true#

wiki_options[:allow_uploads] = true#

wiki_options[:user_icons] = true#

wiki_options[:mathjax] = true#

Cst::WIKIS.each do |w|
  # generate a new application for each word
  gollum_path = File.expand_path(File.dirname(__FILE__)+"/#{w[:disk_path]}") # CHANGE THIS TO POINT TO YOUR OWN WIKI REPO
puts gollum_path
   
app = Sinatra.new(Precious::App)
app.set(:gollum_path, gollum_path)
app.set(:default_markup, :markdown) # set your favorite markup language
app.set(:wiki_options, wiki_options)
  map "/#{w[:web_path]}" do
      use IsAuthMiddleware
      use TaggingMiddleware, :wiki => w
      run app 
  end
end

